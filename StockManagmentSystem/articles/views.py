from django.shortcuts import render, redirect
from .models import Article
from django.contrib.auth.decorators import login_required
from .forms import ArticleForm
from django.http import Http404

def article_home_view(request, id):
    tab = [1, 2, 3, 4, 5]

    context = {"object": id}
    return render(request, "articles/detail.html", context=context)


def article_search_view(request):
    # print(request.GET)
    query = request.GET.get('q')
    #query = query_dict.get("q")  # <input type="text" name='q' method='GET'>
    print(query)
    qs = Article.objects.search(query)
    context = {"object_list": qs}
    return render(request, "articles/search.html", context=context)

@login_required
def article_create_view(request):
    # print(request.POST)
    form= ArticleForm(request.POST or None)
    context = {
        "form": form,
        "article_object": None
    }
    # if request.method == "POST":
    #     form = ArticleForm(request.POST)
    if form.is_valid(): # is valid == is clean
        article_object = form.save()
        context['form'] = ArticleForm()
        # return redirect('article-detail', slug=article_object.slug)
        return redirect(article_object.article_detail_absolute_url())
        context['article_object'] = article_object
        context['created'] = True

    return render(request, "articles/create.html", context=context)

def article_datail_view(request, slug=None):
    article_obj = None
    if slug is not None:
        try:
            article_obj = Article.objects.get(slug=slug)
        except Article.DoesNotExist:
            raise Http404
        except Article.MultipleObjectsReturned:
            article_obj = Article.objects.filter(slug=slug).first()
        except:
            raise Http404
    context = {
        "object": article_obj,
    }
    return render(request, "articles/detail.html", context=context)