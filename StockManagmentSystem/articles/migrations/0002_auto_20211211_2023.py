# Generated by Django 3.2.9 on 2021-12-11 19:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='content',
            field=models.TextField(max_length=120),
        ),
        migrations.AlterField(
            model_name='article',
            name='title',
            field=models.TextField(max_length=30),
        ),
    ]
