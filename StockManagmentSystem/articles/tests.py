from django.test import TestCase
from .models import Article
from .utils import slugify_instance_title
from django.utils.text import slugify
# Create your tests here.

class ArtileTestCase(TestCase):
    def setUp(self):
        self.number_of_articles = 50
        for _ in range(0, self.number_of_articles):
            Article.objects.create(title='Hello shrek', content='hello shrek content')

    def test_queryset_exists(self):
        qs = Article.objects.all()
        self.assertTrue(qs.exists())
    
    def test_queryset_count(self):
        qs = Article.objects.all()
        self.assertEqual(qs.count(), self.number_of_articles)
        
    def test_hello_world_unique_slug(self):
        qs = Article.objects.exclude(slug__iexact='hello-shrek')
        print(qs.values_list('slug'))
        for obj in qs: 
            title = obj.title
            slug = obj.slug
            slugified_title = slugify(title)
            self.assertNotEqual(slug, slugified_title)

    def test_slugify_instance_title(self):
        obj = Article.objects.all().last()
        new_slugs = []
        for i in range(0, 25):
            instance = slugify_instance_title(obj, save=False)
            print(instance)
            new_slugs.append(instance.slug)
        unique_slugs = list(set(new_slugs))
        self.assertEqual(len(new_slugs), len(unique_slugs))

    def test_slugify_instance_title_count(self):
        slug_list = Article.objects.all().values_list('slug', flat=True)
        unique_slug_list = list(set(slug_list))
        self.assertEqual(len(slug_list), len(unique_slug_list))

    def test_user_added_slug_unique(self):
        # slug_list = Article.objects.all()
        default_slug = Article.objects.filter(id=1)[0]
        Article.objects.filter(id=2).update(slug = 'hello-shreksss')
        updated_slug = Article.objects.filter(id=2)[0]
        # print(slug_list)
        print(default_slug.slug)
        print(updated_slug.slug)
        # try:
        #     not_unique_slug = Article.objects.filter(id=2).update(slug = 'hello-shrek')
        # except:
        #     unique_slug = slugify_instance_title(Article.objects.filter(id=2)[0], save=False)
            # Article.objects.filter(id=2).update(slug = 'hello-shrek')
        self.assertNotEqual(default_slug.slug, updated_slug.slug)

    def test_article_search_manager(self):
        qs = Article.objects.search(query='Hello shrek')
        self.assertEqual(qs.count(), self.number_of_articles)
        qs = Article.objects.search(query='shrek')
        self.assertEqual(qs.count(), self.number_of_articles) 
        qs = Article.objects.search(query='shrek content')
        self.assertEqual(qs.count(), self.number_of_articles) 
