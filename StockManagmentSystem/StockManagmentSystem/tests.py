from django.test import TestCase
from django.conf import settings
from django.contrib.auth.password_validation import validate_password


class StockManagmentSystemTest(TestCase):
    def test_secret_key_strength(self):
        # self.assertNotEqual(settings.SECRET_KEY, "abc123")
        try:
            is_strong = validate_password(settings.SECRET_KEY)
        except Exception as e:
            msg = f"Weak SECRET KEY {e.messages}"
            self.fail(msg)
