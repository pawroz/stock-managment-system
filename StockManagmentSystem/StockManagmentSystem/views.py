from django.http import HttpResponse
from django.shortcuts import render
from articles.models import Article
from django.template.loader import render_to_string


def home_view(request, *args, **kwargs):
    article_obj = Article.objects.all().first()
    article_queryset = Article.objects.all()
    context = {
        "object_list": article_queryset,
        "object": article_obj,
    }
    # html_string = render_to_string("home.html", context=context)
    # article_obj = Articles.objects
    return render(request, "home.html", context=context)
